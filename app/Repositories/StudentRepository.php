<?php

namespace App\Repositories;

use App\Models\Student;

class StudentRepository implements StudentRepositoryInterface
{

   public function findById(int $id){
       return Student::find($id);
   }

   public function save(array $attributes){
       $model = new Student;
       $key = $model->getKeyName();
       if(isset($attributes[$key]) && !empty($attributes[$key])){
           $model = $model->find($attributes[$key]);
       }
       $model->fill($attributes);
       $model->save();
       return $model;
   }

   public function getCollection(){
        return Student::select(['id','first_name','last_name','gender','join_year'])->get();
   }

   public function getPaginate($limit){
        return Student::select(['students.id','first_name','last_name','gender','join_year','class_rooms.name as class_room_name','class_room_id','teachers.name as teacher_name'])
        ->leftJoin('class_rooms','class_rooms.id','=','students.class_room_id')
        ->leftJoin('teachers','class_rooms.teacher_id','=','teachers.id')
        ->orderBy('students.id','desc')->paginate($limit);
   }

   public function delete(int $id){
       return Student::destroy($id);
   }
}
