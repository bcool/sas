<?php
namespace App\Repositories;
interface StudentRepositoryInterface {
    public function findById(int $id);
    public function getCollection();
    public function save(array $attributes);
    public function delete(int $id);
}