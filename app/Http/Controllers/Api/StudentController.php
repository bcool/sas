<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Repositories\StudentRepository;

class StudentController extends Controller
{
    public function __construct(StudentRepository $student){
        $this->student = $student;
    }

    public function index(Request $request){
        $students = $this->student->getPaginate($request->input('limit',50));
        return response()->json($students);
    }

    public function show($id){
        $response = [];
        if($id){
            $student = $this->student->findById($id);
            $response = $student->only(['id','first_name','last_name','gender','join_year']);
        }
        return response()->json($response);
    }

    public function create(Request $request){
        //dd($request->all());
        $validator = $this->validator($request);
        if ($validator->fails()) {
            return response()->json(['error'=>[
                'code'=>'400',
                'error'=>'Invalid Request.',
                'description'=>$validator->errors()->all(),
            ]],400);
        }else{
            $student = $this->student->save([
                'first_name'=>$request->input('first_name'),
                'last_name'=>$request->input('last_name'),
                'gender'=>$request->input('gender'),
                'join_year'=>$request->input('join_year'),
                'class_room_id'=>$request->input('class_room_id')
            ]);
            if($student){
                return response()->json($student->only(['id','first_name','last_name','gender','join_year']));
            }else{
                return response()->json(['error'=>[
                    'code'=>'500',
                    'error'=>'Opps! Something went wrong! Please try again',
                    'description'=>'',
                ]],500);
            }
        }
    }

    public function update($id,Request $request){
        $validator = $this->validator($request,$id);
        if ($validator->fails()) {
            return response()->json(['error'=>[
                'code'=>'400',
                'error'=>'Invalid Request.',
                'description'=>$validator->errors()->all(),
            ]],400);
        }else{
            $student = $this->student->save([
                'id'=>$id,
                'first_name'=>$request->input('first_name'),
                'last_name'=>$request->input('last_name'),
                'gender'=>$request->input('gender'),
                'join_year'=>$request->input('join_year'),
                'class_room_id'=>$request->input('class_room_id')
            ]);
            if($student){
                return response()->json($student->only(['id','first_name','last_name','gender','join_year']));
            }else{
                return response()->json(['error'=>[
                    'code'=>'500',
                    'error'=>'Opps! Something went wrong! Please try again',
                    'description'=>'',
                ]],500);
            }
        }
    }

    public function destroy($id){
        if($id){
            if($this->student->delete($id)){
                return response()->json('User deleted successfully');
            }else{
                return response()->json(['error'=>[
                    'code'=>'500',
                    'error'=>'Opps! Something went wrong! Please try again',
                    'description'=>'',
                ]],500);
            }
        }else{
            return response()->json(['error'=>[
                'code'=>'400',
                'error'=>'Invalid Request.',
                'description'=>'',
            ]],400);
        }
    }

    private function validator(Request $request,$id = null){
        $data = $request->all();
        if($id > 0){
            //update
            $rules = [
                'first_name' => 'required',
                'last_name' => 'required',
                'gender'=>'required',
                'join_year'=>'required',
                'class_room_id'=>'required'
            ];
            return Validator::make($data,$rules);
        }else{
            //create
            return Validator::make($data, [
                'first_name' => 'required',
                'last_name' => 'required',
                'gender'=>'required',
                'join_year'=>'required',
                'class_room_id'=>'required'
            ]);
        }

    }
}