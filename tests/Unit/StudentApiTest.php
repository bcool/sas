<?php

namespace Tests\Unit;
use App\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class StudentApiTest extends TestCase
{
    //use WithoutMiddleware;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStudentIndexUnauthorize()
    {
        $response = $this->json('GET', 'api/v.1.0.0/students');
        $response
            ->assertStatus(401);
    }

    public function testStudentIndexSuccess()
    {
        $this->withoutMiddleware();
        $response = $this->json('GET', 'api/v.1.0.0/students');
        $response
            ->assertStatus(200);
    }

    public function testStudentCreateUnauthorize()
    {
        $response = $this->json('POST', 'api/v.1.0.0/students');
        $response
            ->assertStatus(401);
    }

    public function testStudentCreateSuccess()
    {
        $this->withoutMiddleware();
        $response = $this->json('POST', 'api/v.1.0.0/students',[
            'first_name'=>'test',
            'last_name'=>'tyr',
            'gender'=>'M',
            'join_year'=>2020,
            'class_room_id'=>1
            ]);
        $response
            ->assertStatus(200)
            ->assertJson(['first_name'=> "test", 'last_name'=> "tyr", 'gender'=> "M", 'join_year'=>2020]);
    }
    public function testStudentUpdateUnauthorize()
    {
        $response = $this->json('PUT', 'api/v.1.0.0/students/1');
        $response
            ->assertStatus(401);
    }

    public function testStudentUpdateSuccess()
    {
        $this->withoutMiddleware();
        $response = $this->json('PUT', 'api/v.1.0.0/students/2',[
            'first_name'=>'test',
            'last_name'=>'tyr',
            'gender'=>'M',
            'join_year'=>2020,
            'class_room_id'=>1
            ]);
        $response
            ->assertStatus(200)
            ->assertJson(['first_name'=> "test", 'last_name'=> "tyr", 'gender'=> "M", 'join_year'=>2020]);
    }
    public function testStudentDeleteUnauthorize()
    {
        $response = $this->json('DELETE', 'api/v.1.0.0/students/1');
        $response
            ->assertStatus(401);
    }

    public function testAuthLoginFail()
    {
        //$user = factory(User::class)->create();
        $response = $this->json('POST', 'api/v.1.0.0/auth/login',['email','tes@gmail.com','wrong']);
        $response
            ->assertStatus(400);
    }

    public function testAuthRegister()
    {
        $response = $this->json('POST', 'api/v.1.0.0/auth/register',['email'=>'test'.rand().'@gmail.com','name'=>'test1','password'=>'123456','password_confirmation'=>'123456']);
        $response
            ->assertStatus(201);
    }

    public function testAuthLoginSuccess()
    {
        $response = $this->json('POST', 'api/v.1.0.0/auth/login',['email'=>'abc@gmail.com','password'=>'123456']);
        $response
            ->assertStatus(200);
    }

}
