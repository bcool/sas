<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['namespace'=>'Api','prefix'=>'v.1.0.0'],function (){
    Route::post('auth/login', 'AuthController@authenticate');
    Route::post('auth/register', 'AuthController@register');

    Route::group(['middleware' => ['jwt.verify']], function() {
        Route::get('/students', 'StudentController@index');
        Route::get('/students/{id}', 'StudentController@show');
        Route::post('/students', 'StudentController@create');
        Route::put('/students/{id}', 'StudentController@update');
        Route::delete('/students/{id}', 'StudentController@destroy');
    });
});
