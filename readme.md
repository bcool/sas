## SAS Application

> SAS web application

#### 1. How to setup


###### 1.1 Clone repository 
```bash
git clone {repository-path} sas
```
###### 1.2 Configurations
> Edit .env file and set database connection details.

```bash
cd sas
composer install
php artisan migrate:refresh --seed
php artisan serve
```

###### 1.3 You can access application from [URL]http://localhost:8000
> use email:abc@sas.com and password:123456 login to system. You can register new user using following REST with params.
```bash
curl -X POST \
  http://localhost:8000/api/v.1.0.0/auth/register 
  -d 'name=test&email=ab2%40gmail.com&password=123456&password_confirmation=123456'
  ```
