import config from '../config';
import axios from 'axios';
import LocalStorageService from './LocalStorageService';
const user = LocalStorageService.getUsers();
const token = LocalStorageService.hasUser()?user.token:null;

const http = axios.create({
    baseURL: config.API_BASE_URL,
    headers: {'Authorization': `Bearer ${token}`}
  })
export default class UserServices{
    static save(user) {
        if(user.id){
            return http.put(`students/${user.id}`,user)
            .then(res=>{
               return res;
            }).catch(error=>{
               throw error;
            });
        }else{
            return http.post('students',user)
            .then(res=>{
               return res;
            }).catch(error=>{
               throw error;
            });
        }

    }

    static getUsers(page,limit){
        return http.get(`students?page=${page}&limit=${limit}`)
        .then(res => {
            return res;
        }).catch(error=>{
            throw error;
        })
    }

    static deleteUser(id){
        return http.delete(`students/${id}`)
        .then(res => {
            return res;
        }).catch(error=>{
            throw error;
        })
    }
}