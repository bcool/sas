import config from '../config';
import axios from 'axios';
const http = axios.create({
    baseURL: config.API_BASE_URL
  })
export default class AuthServices{
    static login(credential) {
        return http.post(`auth/login`,credential)
        .then(res=>{
           return res;
        }).catch(error=>{
           throw error;
        });
    }
}