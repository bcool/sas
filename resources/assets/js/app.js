
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');
import Vue from 'vue';
import Vuetify from 'vuetify';
import VueRouter from 'vue-router';
import {routes} from './routes';
import MainApp from './components/MainApp.vue';
import LocalStorageService from './services/LocalStorageService';

Vue.use(VueRouter)
Vue.use(Vuetify);
Vue.prototype.$bus = new Vue();

const router = new VueRouter({
    routes:routes,
    mode:'history'
});

router.beforeEach((to,from,next)=>{
    console.log(to.path);
    const requireAuth = to.matched.some(record => record.meta.auth);
    const isAuth = LocalStorageService.hasUser();
    console.log('isAuth',isAuth);
    if(requireAuth && !isAuth){
        next('/login');
    }else if(to.path == '/login' && isAuth){
        next('/')
    }else{
        next();
    }
    
})
//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
    router:router,
    components:{
        MainApp
    }
});
